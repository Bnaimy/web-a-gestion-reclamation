package com.spring.boot.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.spring.boot.service.UserService;

import com.spring.boot.model.Role;
import com.spring.boot.model.User;
import com.spring.boot.repository.RoleRespository;
import com.spring.boot.repository.UserRespository;
import java.util.Arrays;
import java.util.HashSet;
import javax.sql.DataSource;
@Service
public class UserServiceImpl implements UserService {
 
 @Autowired
 private UserRespository userRepository;
 
 @Autowired
 private RoleRespository roleRespository;
 
 @Autowired
 private BCryptPasswordEncoder bCryptPasswordEncoder;

private DataSource dataSource;
@Override
 public User findUserByEmail(String email) {
  return userRepository.findByEmail(email);
 }

 @Override
 public void saveUser(User user) {
  user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
  user.setActive(1);
  Role userRole = roleRespository.findByRole("ADMIN");
  user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
  userRepository.save(user);
 }
 public void setDataSource(DataSource dataSource) {
     this.dataSource = dataSource;
     new JdbcTemplate(dataSource);
}
@Override
public void saveReclamations(String civilite, String nom, String prenom, String Email, String tele, String adresse,
		String codepostal,String ville , String reclamations,String comments) {
	 JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
     jdbcTemplate.update("INSERT INTO reclamations(civilite,nom,prenom,Email,tele,adresse,codepostal,ville,reclamations,comments)VALUES(?,?,?,?,?,?,?,?,?,?)",civilite,nom,prenom,Email,tele,adresse,codepostal,ville,reclamations,comments );	
}
@Override
public void consulteReclamation(String civilite,String nom,String prenom,String Email,String tele,String adresse,String codepostal,String ville,String reclamations,String comments)
{
	JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
    jdbcTemplate.update("SELECT *  FROM reclamations");
}
}
