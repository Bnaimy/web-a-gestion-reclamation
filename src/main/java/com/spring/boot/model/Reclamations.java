package com.spring.boot.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
@Entity
@Table(name = "Reclamations")
public class Reclamations {
	@Id
 @Column(name = "civilite")
 private String civilite;
 @Column(name = "nom")
 private String nom; 
 @Column(name = "prenom")
 private String prenom;
 @Column(name = "Email")
 private String Email;
 @Column(name = "tele")
 private String tele;
 @Column(name = "address")
 private String address;
 @Column(name = "codepostal")
 private String codepostal;
 @Column(name = "ville")
 private String ville;
 @Column(name = "reclamation")
 private String reclamation;
 @Column(name = "comments")
 private String comments;

 public String getcivilite() {
  return civilite;
 }

 public void setcivilite(String civilite) {
  this.civilite = civilite;
 }

 public String getnom() {
  return nom;
 }

 public void setnom(String nom) {
  this.nom = nom;
 }

 public String getprenom() {
  return prenom;
 }

 public void setprenom(String prenom) {
  this.prenom = prenom;
 }

 public String getEmail() {
  return Email;
 }

 public void setEmail(String Email) {
  this.Email = Email;
 }
 public String gettele() {
	  return tele;
	 }

	 public void settele(String tele) {
	  this.tele = tele;
	 }
	 public String getaddress() {
		  return address;
		 }

		 public void setaddress(String address) {
		  this.address = address;
		 }
		 public String getcodepostal() {
			  return codepostal;
			 }

			 public void setcodepostal(String codepostal) {
			  this.codepostal = codepostal;
			 }
			 public String getville() {
				  return ville;
				 }

				 public void setville(String ville) {
				  this.ville = ville;
				 }
				 public String getreclamation() {
					  return reclamation;
					 }

					 public void setreclamation(String reclamation) {
					  this.reclamation = reclamation;
					 }
					 public void setmessage(String comments) {
						  this.comments = comments;
						 }
						 public String getmessage() {
							  return comments;
							 }
					

}

